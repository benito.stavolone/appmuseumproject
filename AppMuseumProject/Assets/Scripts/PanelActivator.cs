﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelActivator : MonoBehaviour
{
    [SerializeField] private GameObject panelToActivate;

    public GameObject PanelToActivate
    {
        set { panelToActivate = value; }
    }


    public void OnButtonClick()
    {
        UIManager.instance.ActivePanel(panelToActivate);
    }
}
