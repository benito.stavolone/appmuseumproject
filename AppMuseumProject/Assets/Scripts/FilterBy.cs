﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FilterBy : MonoBehaviour
{
    [SerializeField] private PiecesTagger[] allWeapons;

    [SerializeField] private Transform transformSlider;

    [SerializeField] private TMP_Dropdown myDropDownList;
    [SerializeField] private TMP_Dropdown myDropDownListByAge;

    public void OnChangeValue()
    {
        for(int i=0;i< allWeapons.Length; i++)
        {            
            switch (myDropDownList.value)
            {
                case 0:
                    allWeapons[i].gameObject.SetActive(true);
                    break;
                case 1:
                    if (allWeapons[i].myTipology != PiecesTagger.Tipology.Armors)
                        allWeapons[i].gameObject.SetActive(false);
                    else
                        allWeapons[i].gameObject.SetActive(true);
                    break;
                case 2:
                    if (allWeapons[i].myTipology != PiecesTagger.Tipology.Swords)
                        allWeapons[i].gameObject.SetActive(false);
                    else
                        allWeapons[i].gameObject.SetActive(true);
                    break;
                case 3:
                    if (allWeapons[i].myTipology != PiecesTagger.Tipology.FireWeapons)
                        allWeapons[i].gameObject.SetActive(false);
                    else
                        allWeapons[i].gameObject.SetActive(true);
                    break;
                case 4:
                    if (allWeapons[i].myTipology != PiecesTagger.Tipology.Plaques)
                        allWeapons[i].gameObject.SetActive(false);
                    else
                        allWeapons[i].gameObject.SetActive(true);
                    break;
            }

            SecondFilter(i);
        }

        
    }

    private void SecondFilter(int i)
    {
        switch (myDropDownListByAge.value)
        {
            case 0:
                //allWeapons[i].gameObject.SetActive(true);
                break;
            case 1:
                if (allWeapons[i].gameObject.activeSelf && allWeapons[i].myPeriod != PiecesTagger.Period.Modern)
                    allWeapons[i].gameObject.SetActive(false);
                break;
            case 2:
                if (allWeapons[i].gameObject.activeSelf && allWeapons[i].myPeriod != PiecesTagger.Period.MiddleAges)
                    allWeapons[i].gameObject.SetActive(false);
                break;
            case 3:
                if (allWeapons[i].gameObject.activeSelf && allWeapons[i].myPeriod != PiecesTagger.Period.Contemporary)
                    allWeapons[i].gameObject.SetActive(false);
                break;
        }
    }
}
