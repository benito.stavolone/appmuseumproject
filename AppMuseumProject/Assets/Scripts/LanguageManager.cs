﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LanguageManager : MonoBehaviour
{
    public TMP_Dropdown languageDropdown;

    public MyTextToConvert[] myTextToConvert;

    private string[] standardLanguage;

    private void Awake()
    {
        standardLanguage = new string[10];

        for (int i = 0; i < myTextToConvert.Length; i++)
            standardLanguage[i] = myTextToConvert[i].mytext.text;
    }

    private void OnEnable()
    {
        SetLanguage();
    }

    public void OnValueChange()
    {
        SetLanguage();
    }

    private void SetLanguage()
    {
        if (languageDropdown.value == 1)
        {
            for (int i = 0; i < myTextToConvert.Length; i++)
                myTextToConvert[i].mytext.text = myTextToConvert[i].translation;
        }
        else
        {
            for (int i = 0; i < myTextToConvert.Length; i++)
                myTextToConvert[i].mytext.text = standardLanguage[i];
        }
    }
}

[System.Serializable]
public class MyTextToConvert
{
    public TextMeshProUGUI mytext;
    public string translation;
}
