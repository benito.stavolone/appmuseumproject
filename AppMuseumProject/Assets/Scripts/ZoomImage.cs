﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomImage : MonoBehaviour
{
    public Image imageToZoom;

    public Image[] zoomImage;

    private void OnEnable()
    {
        for(int i = 0; i < zoomImage.Length; i++)
        {
            zoomImage[i].gameObject.SetActive(false);
        }
    }

    public void Zooming()
    {
        int index;

        if (imageToZoom.sprite.texture.width > imageToZoom.sprite.texture.height)
            index = 1;
        else
            index = 0;

        zoomImage[index].gameObject.SetActive(true);
        zoomImage[index].sprite = imageToZoom.sprite;
    }
}
