﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;

public class QuizManager : MonoBehaviour
{
    public GameObject welcomePanel;

    public GameObject congratulationsPanel;
    public TextMeshProUGUI awardsText;

    public Slider awards;

    public Question[] questions;
    private List<Question> unanSwared;
    private Question currentQuestion;
    private bool isMyfirstTime = true;

    public TMP_Dropdown languageDropdown;

    public Color correctColor;

    [Header("UIComponents")]

    [SerializeField] private TextMeshProUGUI questionText;
    [SerializeField] private float timeBetweenQuestions = 1f;

    [SerializeField] private Image trueButton;
    [SerializeField] private Image falseButton;

    [SerializeField] private TextMeshProUGUI trueButtonText;
    [SerializeField] private TextMeshProUGUI falseButtonText;

    private Color initialTrueButtonColor;
    private Color initialFalseButtonColor;

    public Material newMaterial;
    public Material wrongMaterial;

    private void OnEnable()
    {
        if (isMyfirstTime)
        {
            welcomePanel.SetActive(true);
        }
        else
        {
            welcomePanel.SetActive(false);
        }
            

        initialFalseButtonColor = falseButton.color;
        initialTrueButtonColor = trueButton.color;

        if (unanSwared == null || unanSwared.Count == 0)
            unanSwared = questions.ToList<Question>();

        SetCurrentQuestion();
    }

    private void OnRefresh()
    {
        if (unanSwared == null || unanSwared.Count == 0)
        {
            congratulationsPanel.SetActive(true);
            awardsText.text = (Mathf.RoundToInt((awards.value * 50 )/ 1.4f)).ToString() + "%";
            trueButton.GetComponent<Button>().interactable = false;
            falseButton.GetComponent<Button>().interactable = false;
            isMyfirstTime = false;
        }
        else
            SetCurrentQuestion();
    }

    private void SetCurrentQuestion()
    {

        int randomQuestionIndex = Random.Range(0, unanSwared.Count);
        currentQuestion = unanSwared[randomQuestionIndex];

        if (languageDropdown.value == 0)
            questionText.text = currentQuestion.question;
        else
            questionText.text = currentQuestion.italianQuestion;
    }

    IEnumerator TransitionToNexrQuestion()
    {
        unanSwared.Remove(currentQuestion);

        yield return new WaitForSeconds(timeBetweenQuestions);

        OnReset();

        OnRefresh();
    }

    public void UserSelectTrue()
    {
        if (currentQuestion.isTrue)
        {
            trueButton.color = newMaterial.color;
            awards.value += 0.2f;
        }
        else
        {
            falseButton.color = newMaterial.color;
            trueButton.color = wrongMaterial.color;
        }
            

        StartCoroutine(TransitionToNexrQuestion());
    }

    public void UserSelectFalse()
    {
        if (!currentQuestion.isTrue)
        {
            awards.value += 0.2f;
            falseButton.color = newMaterial.color;
        }          
        else
        {
            trueButton.color = newMaterial.color;
            falseButton.color = wrongMaterial.color;
        }

        StartCoroutine(TransitionToNexrQuestion());
    }

    private void OnReset()
    {
        trueButton.color = initialTrueButtonColor;
        falseButton.color = initialFalseButtonColor;
    }

    public void ContinueButton()
    {
        welcomePanel.SetActive(false);

        trueButton.GetComponent<Button>().interactable = true;
        falseButton.GetComponent<Button>().interactable = true;
    }
}
