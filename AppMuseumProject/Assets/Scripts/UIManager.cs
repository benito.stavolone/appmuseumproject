﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : PeristentSingleton<UIManager>
{
    [SerializeField] private GameObject[] panelList;

    public void ActivePanel(GameObject panelToActivate)
    {
        for(int i = 0; i < panelList.Length; i++)
        {
            if (panelList[i].name == panelToActivate.name)
                panelList[i].SetActive(true);
            else
                panelList[i].SetActive(false);
        }
    }
}
