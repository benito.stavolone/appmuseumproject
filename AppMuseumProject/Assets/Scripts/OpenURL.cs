﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenURL : MonoBehaviour
{
    [SerializeField] private string URLtoOpen;

    public void OpenLink()
    {
        Application.OpenURL(URLtoOpen);
    }
}
