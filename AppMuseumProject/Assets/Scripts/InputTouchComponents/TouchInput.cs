using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TouchInput
{
	// "Core" Variables.
	
	public static Vector2[] tapPositions = new Vector2[2];
	private static Vector2[] swipePositions = new Vector2[2];
	
	// "Offset" Variables.

	private static float offsetTap = 15.0F;
	private static float offsetSwipe = 40.0F;

	// "Flag" Variables.
	
	private static bool fTapAllowed = false;
	private static bool fSwipeAllowed = false;

	// "Other" Variables.

	private static float tempX = 0.0F;
	private static float tempY = 0.0F;

	//LongTap

	private static float timePressed = 0;
	private static float timeLastPress = 0;
	private static float timerLongTap = 3;

	//Drag

	private static Vector3 offset;
	private static float distance;
	private static bool dragging = false;

	//Swipe

	private static float startTime;
	private static float endTime;
	private static float swipeTime;

	private static Vector2 swipeStartPos;
	private static Vector2 swipeEndPos;
	private static float swipeDistance;

	private static float maxSwipeTime = 0.5f;
	private static float minSwipeDistance = 100;

	// "Core" Methods.

	public static void ProcessTouches()
	{
		if (Input.touchCount > 0) 
		{
			Touch touch = Input.touches[0];

			if (touch.phase == TouchPhase.Began) 
			{
				TouchInput.tapPositions[0]   = touch.position;
				TouchInput.swipePositions[0] = touch.position;
			}
			else if (touch.phase == TouchPhase.Canceled)
			{
				TouchInput.ResetPositions();
			}
			else if (touch.phase == TouchPhase.Ended) 
			{
				TouchInput.tapPositions[1]   = touch.position;
				TouchInput.swipePositions[1] = touch.position;

				TouchInput.fTapAllowed   = true;
				TouchInput.fSwipeAllowed = true;
			}
			else if (touch.phase == TouchPhase.Moved) 
			{

			}
			else if (touch.phase == TouchPhase.Stationary) 
			{

			}
		}

	}

	private static void ResetPositions()
	{
		TouchInput.tapPositions  = new Vector2[2];

		TouchInput.fTapAllowed   = false;
		TouchInput.fSwipeAllowed = false;
	}
	
	public static bool Tap()
	{
		bool result = false;

		if (TouchInput.fTapAllowed)
		{
			TouchInput.tempX = Mathf.Abs(TouchInput.tapPositions[0].x - TouchInput.tapPositions[1].x);
			TouchInput.tempY = Mathf.Abs(TouchInput.tapPositions[0].y - TouchInput.tapPositions[1].y);

			if (tempX <= TouchInput.offsetTap && tempY <= TouchInput.offsetTap)
			{
				result = true;
			}

			TouchInput.tapPositions = new Vector2[2];
			TouchInput.fTapAllowed  = false;
		}

		return result;
	}

	public static bool LongTap(float timer)
    {
		bool result = false;
		timerLongTap = timer;

		if (Input.touchCount > 0)
		{
			if (Input.touches[0].phase == TouchPhase.Began)
				timePressed = Time.time - timeLastPress;

			if (Input.touches[0].phase == TouchPhase.Stationary || Input.touches[0].phase == TouchPhase.Moved)
			{
				if (Time.time - timePressed > timerLongTap)
					result = true;
			}

			if (Input.touches[0].phase == TouchPhase.Ended)
				timeLastPress = 0;
		}

		return result;
	}

	public static Vector3 Drag(GameObject objectToDrag)
    {
		Vector3 newVector = objectToDrag.transform.position;

		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Began)
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(touch.position);
				if (Physics.Raycast(ray, out hit) && (hit.collider.CompareTag("Draggable")))
				{
					objectToDrag = hit.transform.gameObject;
					distance = hit.transform.position.z - Camera.main.transform.position.z;
					dragging = true;
				}
			}

			if (dragging && touch.phase == TouchPhase.Moved)
			{
				newVector = new Vector3(touch.position.x, touch.position.y, objectToDrag.transform.position.z);
				newVector = Camera.main.ScreenToWorldPoint(newVector);

			}

			if (dragging && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled))
			{
				dragging = false;
			}

		}

		return newVector;
	}

	public static bool Swipe()
	{
		bool result = false;

		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Began)
			{
				startTime = Time.time;
				swipeStartPos = touch.position;
			}

			else if (touch.phase == TouchPhase.Ended)
			{
				endTime = Time.time;
				swipeEndPos = touch.position;

				swipeTime = endTime - startTime;
				swipeDistance = (swipeEndPos - swipeStartPos).magnitude;

				if (swipeTime < maxSwipeTime && swipeDistance > minSwipeDistance)
				{
					result = true;
				}
			}
		}

		return result;
	}

	public static bool SwipeRight()
    {
		bool result = false;

        if (Swipe())
        {
			Vector2 distance = swipeEndPos - swipeStartPos;

			float xDistance = Mathf.Abs(distance.x);
			float yDistance = Mathf.Abs(distance.y);

			if (xDistance > yDistance)
			{
				if (swipeEndPos.x > swipeStartPos.x)
					result = true;
			}
		}

		return result;
    }

	public static bool SwipeLeft()
	{
		bool result = false;

		if (Swipe())
		{
			Vector2 distance = swipeEndPos - swipeStartPos;

			float xDistance = Mathf.Abs(distance.x);
			float yDistance = Mathf.Abs(distance.y);

			if (xDistance > yDistance)
			{
				if (swipeEndPos.x < swipeStartPos.x)
					result = true;
			}
		}

		return result;
	}

	public static bool SwipeUp()
	{
		bool result = false;

		if (Swipe())
		{
			Vector2 distance = swipeEndPos - swipeStartPos;

			float xDistance = Mathf.Abs(distance.x);
			float yDistance = Mathf.Abs(distance.y);

			if (xDistance < yDistance)
			{
				if (swipeEndPos.y > swipeStartPos.y)
					result = true;
			}
		}

		return result;
	}

	public static bool SwipeDown()
	{
		bool result = false;

		if (Swipe())
		{
			Vector2 distance = swipeEndPos - swipeStartPos;

			float xDistance = Mathf.Abs(distance.x);
			float yDistance = Mathf.Abs(distance.y);

			if (xDistance < yDistance)
			{
				if (swipeEndPos.y < swipeStartPos.y)
					result = true;
			}
		}

		return result;
	}

	public static float Pinch()
	{
		float result = 0;

		if (Input.touchCount == 2)
		{
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).sqrMagnitude;
			float currentMagnitude = (touchZero.position - touchOne.position).sqrMagnitude;

			float difference = currentMagnitude - prevMagnitude;
			result = difference;
		}

		return result;
	}

	public static Vector3 GetSwipeEndPosition()
	{
		Ray touchposition = Camera.main.ScreenPointToRay(swipeEndPos);
		RaycastHit hit;
		bool isHit = Physics.Raycast(touchposition, out hit, Mathf.Infinity);

		return hit.point;
	}

	public static Vector3 GetSwipeStartPosition()
	{
		Ray touchposition = Camera.main.ScreenPointToRay(swipeStartPos);
		RaycastHit hit;
		bool isHit = Physics.Raycast(touchposition, out hit, Mathf.Infinity);

		return hit.point;
	}

	public static Vector3 GetTapPosition()
    {
		Ray touchposition = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
		RaycastHit hit;
		bool isHit = Physics.Raycast(touchposition, out hit, Mathf.Infinity);

		return hit.point;
	}
}
