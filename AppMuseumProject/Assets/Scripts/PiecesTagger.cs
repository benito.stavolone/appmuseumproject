﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using TMPro;

public class PiecesTagger : MonoBehaviour
{
    public TMP_Dropdown languageDropdown;

    public enum Tipology
    {
        Armors,
        Swords,
        FireWeapons,
        Plaques
    }

    public enum Period
    {
        None,
        MiddleAges,
        Modern,
        Contemporary
    }

    public Tipology myTipology;
    public Period myPeriod;

    [Header("Title reference")]

    [SerializeField] private string Name;
    public string NameEng;
    [SerializeField] private TextMeshProUGUI nameText;

    public GameObject myPanel;
    public TextMeshProUGUI titletext;

    [Header("Image reference")]

    public Sprite myImage;
    public Sprite fullImage;
    public Image ImageContainer;
    public Image fullImageContainer;

    [Header("Description reference")]

    public string myDescription;
    public string myDescriptionENG;
    public TextMeshProUGUI myTextReference;

    private void OnEnable()
    {
        if (languageDropdown.value == 1)
            nameText.text = Name;
        else
            nameText.text = NameEng;
    }

    public void ActivateMyPanel()
    {
        UIManager.instance.ActivePanel(myPanel);

        if (languageDropdown.value == 1)
        {
            titletext.text = Name;
            myTextReference.text = myDescription;
        }
        else
        {
            titletext.text = NameEng;
            myTextReference.text = myDescriptionENG;
        }
            
        ImageContainer.sprite = myImage;
        fullImageContainer.sprite = fullImage;
    }
}
