﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBackPanel : MonoBehaviour
{
    public PanelActivator panelActivatorReference;

    public GameObject realPanelToActivate;

    public void OnClick()
    {
        panelActivatorReference.PanelToActivate = realPanelToActivate;
    }
}
