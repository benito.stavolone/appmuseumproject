﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapRotating : MonoBehaviour
{
    public float rotatespeed = 10f;

    public Slider horizontalSlider;
    public Slider verticalSlider;

    void Update()
    {
        transform.localEulerAngles = new Vector3(verticalSlider.value * rotatespeed, horizontalSlider.value * rotatespeed, 0.0f);
    }
}
