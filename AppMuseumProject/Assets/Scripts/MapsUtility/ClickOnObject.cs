﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickOnObject : MonoBehaviour
{
    private GameObject myImage;

    // Update is called once per frame
    void Update()
    {
        if ((Input.touchCount > 0) && (Input.touchCount < 2) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                if (raycastHit.collider.CompareTag("Interactable"))
                {
                    if (myImage != null)
                        myImage.SetActive(false);
                    myImage = raycastHit.collider.gameObject.GetComponent<ImageGetter>().GetImage();
                    myImage.SetActive(true);
                }
                else
                {
                    if (myImage != null)
                        myImage.SetActive(false);
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                if (raycastHit.collider.CompareTag("Interactable"))
                {
                    if(myImage!=null)
                        myImage.SetActive(false);
                    myImage = raycastHit.collider.gameObject.GetComponent<ImageGetter>().GetImage();
                    myImage.SetActive(true);
                }
                else
                {
                    if (myImage != null)
                        myImage.SetActive(false);
                }
            }
        }
    }

    public void DisableImage()
    {
        if(myImage!=null)
            myImage.SetActive(false);
    }
}
