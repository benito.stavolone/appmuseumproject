﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapZoomming : MonoBehaviour
{

    private float zoomSpeed = 2; 

    private void Update()
    {
        if (gameObject.activeSelf)
        {
            float distance = TouchInput.Pinch();

            if (distance > 0 && transform.localScale.y < 1.7f)
                transform.localScale += Vector3.one * zoomSpeed * Time.deltaTime;  


            if (distance < 0 && transform.localScale.y > 1)
                transform.localScale -= Vector3.one * zoomSpeed * Time.deltaTime;
        }

        if (gameObject.activeSelf)
        {
            if (Input.mouseScrollDelta.y < 0)
            {
                if (transform.localScale.y > 1)
                    transform.localScale -= Vector3.one * zoomSpeed * Time.deltaTime;
            }

            if (Input.mouseScrollDelta.y > 0)
            {
                if (transform.localScale.y < 1.7f)
                    transform.localScale += Vector3.one * zoomSpeed * Time.deltaTime;
            }
        }
    }
}
