﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TourChosen : MonoBehaviour
{
    public TMP_Dropdown tourDropdown;

    public GameObject objectToDisable;

    private void OnEnable()
    {
        onTourChosen();
    }

    public void onTourChosen()
    {
        if (tourDropdown.value == 0)
        {
            objectToDisable.SetActive(false);
        }
        else
            objectToDisable.SetActive(true);
    }
}
