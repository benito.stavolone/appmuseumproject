﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMoving : MonoBehaviour
{
    Vector3 startPosition;

    private void Update()
    {
        TouchInput.ProcessTouches();

        if (Input.touchCount > 0 && Input.touchCount < 2)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3(transform.position.x + touch.deltaPosition.x * 2 * Time.deltaTime,
                    transform.position.y + touch.deltaPosition.y * 2 * Time.deltaTime,
                    transform.position.z);
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            startPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(1))
        {
            transform.position = new Vector3(transform.position.x + (Input.mousePosition.x - startPosition.x) * Time.deltaTime,
                    transform.position.y + (Input.mousePosition.y - startPosition.y) * Time.deltaTime,
                    transform.position.z);
        }
    }
}
