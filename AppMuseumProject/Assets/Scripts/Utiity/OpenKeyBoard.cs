﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenKeyBoard : MonoBehaviour
{

    TouchScreenKeyboard keyboard;

    public void ShowKeyBoard()
    {
        Debug.Break();
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
    }
}
