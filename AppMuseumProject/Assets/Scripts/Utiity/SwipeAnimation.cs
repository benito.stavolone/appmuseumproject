﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeAnimation : MonoBehaviour
{
    Vector2 scrollPosition;
    Touch touch;

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            TouchInput.ProcessTouches();

            if (Input.touchCount > 0)
            {
                touch = Input.touches[0];
                if (touch.phase == TouchPhase.Moved)
                {
                    scrollPosition.y += touch.deltaPosition.y;
                    scrollPosition.y = Mathf.Clamp(scrollPosition.y, Screen.height/2, Screen.height);
                    transform.position = new Vector2(transform.position.x, scrollPosition.y);
                }
            }
        }
    }
}
