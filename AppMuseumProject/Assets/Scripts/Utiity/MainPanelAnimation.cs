﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPanelAnimation : MonoBehaviour
{

    [SerializeField] private GameObject panelToMove;

    [SerializeField] private Transform panelPosition;
    [SerializeField] private Transform normalPosition;

    private int count = 0;

    private void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            if (count % 2 == 0)
                panelToMove.transform.position = Vector3.Lerp(panelToMove.transform.position, normalPosition.position, 2.5f * Time.deltaTime);
            else
                panelToMove.transform.position = Vector3.Lerp(panelToMove.transform.position, panelPosition.position, 2.5f * Time.deltaTime);
        }
    }

    public void OnButtonClick()
    {
        count++;
    }

    private void OnDisable()
    {
        panelToMove.transform.position = normalPosition.position;
        count = 0;
    }
}
