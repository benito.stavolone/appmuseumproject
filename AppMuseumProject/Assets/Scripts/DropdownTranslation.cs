﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DropdownTranslation : MonoBehaviour
{
    public TMP_Dropdown myDropdown;
    public TMP_Dropdown languageDropdown;

    public string[] translation;

    private string[] standardLanguage;

    // Start is called before the first frame update
    void Awake()
    {
        standardLanguage = new string[10];

        for (int i = 0; i < myDropdown.options.Count; i++)
            standardLanguage[i] = myDropdown.options[i].text;
    }

    private void OnEnable()
    {
        SetLanguage();
    }

    private void SetLanguage()
    {
        if (languageDropdown.value == 1)
        {
            for (int i = 0; i < myDropdown.options.Count; i++)
                myDropdown.options[i].text = translation[i];
        }
        else
        {
            for (int i = 0; i < myDropdown.options.Count; i++)
                myDropdown.options[i].text = standardLanguage[i];
        }
    }
}
