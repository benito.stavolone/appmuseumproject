﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CellContainerController : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI contentText;

    public void AddValue(string name, string content)
    {
        nameText.text = name;
        contentText.text = content;
    }
}
