﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableMessageReviewPopUp : MonoBehaviour
{
    [SerializeField] private GameObject popUpMessage;

    public void OnClickButton()
    {
        popUpMessage.SetActive(true);
    }
}
