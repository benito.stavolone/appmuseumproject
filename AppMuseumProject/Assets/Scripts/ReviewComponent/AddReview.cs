﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AddReview : MonoBehaviour
{

    [SerializeField] private CellContainerController cellContent;
    [SerializeField] private GameObject popUp;
    [SerializeField] private Transform transformSlider;

    public int totalNumber;

    [Header("Reviewer Settings")]

    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI contentText;

    private void AddElementInGrid()
    {
        CellContainerController newObject;

        newObject = Instantiate(cellContent, transformSlider);
        newObject.GetComponent<Image>().color = Random.ColorHSV();
        newObject.AddValue(nameText.text, contentText.text);
    }

    public void PostReview()
    {
        popUp.SetActive(false);
        AddElementInGrid();
    }
}
